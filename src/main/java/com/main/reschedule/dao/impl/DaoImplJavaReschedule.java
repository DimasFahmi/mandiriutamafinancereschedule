package com.main.reschedule.dao.impl;

import org.springframework.stereotype.Repository;
import com.main.reschedule.Conn;
import com.main.reschedule.DbConnection;
import com.main.reschedule.constraint.SQLReschedule;
import com.main.reschedule.dao.DaoJavaReschedule;

@Repository
public class DaoImplJavaReschedule extends DbConnection implements DaoJavaReschedule {

	@Override
	public String Result(Conn cn) throws Exception {
		String listBank = null;
		try {
			String sql = SQLReschedule.Initial_SQL;
			cn.pr = cn.connection.prepareStatement(sql);
			cn.rs = cn.pr.executeQuery();

			while (cn.rs.next()) {
				listBank = cn.rs.getString("nomer");
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			cn.closeStatement();
		}
		return listBank;
	}

}
