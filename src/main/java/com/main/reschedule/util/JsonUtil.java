package com.main.reschedule.util;

import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonUtil {
	private static final Logger logger = LoggerFactory.getLogger(JsonUtil.class);
	protected void encode(HttpServletResponse response, Map<String, Object> map) {
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json");
		
		PrintWriter out;
		
		try {
			out = response.getWriter();
			mapper.writeValue(out, map);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
		protected void encode2(HttpServletResponse response, Map<String, Object> map) {		
			ObjectMapper mapper = new ObjectMapper();			
			response.setContentType("application/json");
			PrintWriter out;
			try {
				out = response.getWriter();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				mapper.writeValue(out, map);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		
		protected void encode2_1(HttpServletResponse response, Object obj) {		
			ObjectMapper mapper = new ObjectMapper();			
			response.setContentType("application/json");
			PrintWriter out;
			try {
				out = response.getWriter();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				mapper.writeValue(out, obj);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		
		protected void encode3(HttpServletResponse response, Map<String, Object> map) {		
			ObjectMapper mapper = new ObjectMapper();			
			response.setContentType("application/json");
			PrintWriter out;
			try {
				out = response.getWriter();
				mapper.setSerializationInclusion(Inclusion.NON_DEFAULT);
				mapper.writeValue(out, map);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
}
