package com.main.reschedule.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.main.reschedule.DbConnection;
import com.main.reschedule.dao.DaoJavaReschedule;
import com.main.reschedule.service.ServiceJavaReschedule;
import com.main.reschedule.Conn;

@Service
public class ServiceImplJavaReschedule extends DbConnection implements ServiceJavaReschedule {

	private static final Logger logger = LoggerFactory.getLogger(DbConnection.class);

//	@Autowired
	private DaoJavaReschedule dao;

	@Autowired
	public void repository(DaoJavaReschedule dao) {
		this.dao = dao;
	}

	@Override
	public String reschedule() throws Exception {
		Conn cn = new Conn();
		String result = null;
		try {
			cn.connection = getConnection();
//			cn.connection.setAutoCommit(false);

			result = dao.Result(cn);
			logger.info(result);
		} catch (Exception e) {
			cn.connection.rollback();
			logger.error(e.getMessage(), e);

		} finally {
			cn.closeConnection();
		}
		
		return result;
	}
}
