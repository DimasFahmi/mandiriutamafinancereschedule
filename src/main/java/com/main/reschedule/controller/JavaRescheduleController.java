package com.main.reschedule.controller;

import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.main.reschedule.service.ServiceJavaReschedule;
import com.main.reschedule.util.JsonUtil;

@RestController
public class JavaRescheduleController extends JsonUtil {
	public static final Logger logger = LoggerFactory.getLogger(JavaRescheduleController.class);

	@Autowired
	private ServiceJavaReschedule serviceJavaReschedule;

	@RequestMapping(value = "/reschedule", method = RequestMethod.GET)
	public String getInitialReschedule(HttpServletResponse response) {
		
		String listBank = null;

		try {
			listBank = serviceJavaReschedule.reschedule();
			logger.info(listBank);
		} catch (Exception e) {
			logger.error(e.getMessage());
			listBank = "Error";
		}
		
		return listBank;
		
	}
}
