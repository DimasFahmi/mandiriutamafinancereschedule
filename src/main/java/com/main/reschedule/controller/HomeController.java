package com.main.reschedule.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.main.reschedule.util.JsonUtil;

@RestController
public class HomeController extends JsonUtil {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public void home(HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Berhasil Menjalankan Reschedule App", true);

		this.encode2(response, map);
	}
}
