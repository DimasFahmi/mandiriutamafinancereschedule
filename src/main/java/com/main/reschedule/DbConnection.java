package com.main.reschedule;

import java.sql.Connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

public class DbConnection {

	@Autowired
	private DataSource dataSource;

	protected static final Logger logger = LoggerFactory.getLogger(DbConnection.class);

	public Connection getConnection() {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return connection;
	}
}
