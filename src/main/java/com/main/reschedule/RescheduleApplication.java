package com.main.reschedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RescheduleApplication {

	public static void main(String[] args) {
		SpringApplication.run(RescheduleApplication.class, args);
	}

}
