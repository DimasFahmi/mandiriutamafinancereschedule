package com.main.reschedule;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

@Configuration
public class BeanConfig {
	@Bean(name = "datasource")
	  public DataSource dataSourcejndi() throws NamingException {
	      JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
	      bean.setJndiName("java:/SSM" ); //
	      bean.setProxyInterface(DataSource.class);
	      bean.setLookupOnStartup(false);
	      bean.afterPropertiesSet();
	      return (DataSource) bean.getObject();
	}
}
